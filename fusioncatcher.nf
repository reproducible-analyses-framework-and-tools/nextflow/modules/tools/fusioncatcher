#!/usr/bin/env Nextflow

process fusioncatcher {
// require:
//   FQS
//   params.starfusion$ctat_ref
//   params.starfusion$starfusion_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'fusioncatcher_container'
  label 'fusioncatcher'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/fusioncatcher"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  path ref
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}*fusioncatcher"), optional: true, emit: fusions

  script:
  """
  fusioncatcher.py \
  -d ${ref} \
  -i ${fq1},${fq2} \
  -o ${dataset}-${pat_name}-${run}.fusioncatcher
  """
}
